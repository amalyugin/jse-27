package ru.t1.malyugin.tm.service;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.api.repository.IProjectRepository;
import ru.t1.malyugin.tm.api.service.IProjectService;
import ru.t1.malyugin.tm.enumerated.Status;
import ru.t1.malyugin.tm.exception.entity.ProjectNotFoundException;
import ru.t1.malyugin.tm.exception.field.*;
import ru.t1.malyugin.tm.model.Project;

public final class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository> implements IProjectService {

    public ProjectService(@NotNull final IProjectRepository projectRepository) {
        super(projectRepository);
    }

    @NotNull
    @Override
    public Project create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (StringUtils.isBlank(name)) throw new NameEmptyException();
        if (StringUtils.isBlank(description)) throw new DescriptionEmptyException();
        return repository.create(userId.trim(), name.trim(), description.trim());
    }

    @NotNull
    @Override
    public Project create(
            @Nullable final String userId,
            @Nullable final String name
    ) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (StringUtils.isBlank(name)) throw new NameEmptyException();
        return repository.create(userId.trim(), name.trim());
    }

    @NotNull
    @Override
    public Project updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (StringUtils.isBlank(id)) throw new IdEmptyException();
        @Nullable final Project project = repository.findOneById(userId.trim(), id.trim());
        if (project == null) throw new ProjectNotFoundException();
        if (!StringUtils.isBlank(name)) project.setName(name.trim());
        if (!StringUtils.isBlank(description)) project.setDescription(description.trim());
        return project;
    }

    @NotNull
    @Override
    public Project updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        @Nullable final Project project = repository.findOneByIndex(userId.trim(), index);
        if (project == null) throw new ProjectNotFoundException();
        if (!StringUtils.isBlank(name)) project.setName(name.trim());
        if (!StringUtils.isBlank(description)) project.setDescription(description.trim());
        return project;
    }

    @NotNull
    @Override
    public Project changeProjectStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (StringUtils.isBlank(id)) throw new IdEmptyException();
        @Nullable final Project project = repository.findOneById(userId.trim(), id.trim());
        if (project == null) throw new ProjectNotFoundException();
        if (status != null) project.setStatus(status);
        return project;
    }

    @NotNull
    @Override
    public Project changeProjectStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        @Nullable final Project project = repository.findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        if (status != null) project.setStatus(status);
        return project;
    }

}