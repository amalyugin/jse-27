package ru.t1.malyugin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.enumerated.Role;
import ru.t1.malyugin.tm.util.TerminalUtil;

public final class UserUnlockCommand extends AbstractUserCommand {

    @NotNull
    private static final String NAME = "user-unlock";

    @NotNull
    private static final String DESCRIPTION = "user unlock";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[USER UNLOCK]");
        System.out.print("ENTER LOGIN: ");
        @NotNull final String login = TerminalUtil.nextLine();
        getUserService().unlockUserByLogin(login);
    }

}