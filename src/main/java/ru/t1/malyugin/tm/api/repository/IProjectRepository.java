package ru.t1.malyugin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.model.Project;

public interface IProjectRepository extends IUserOwnerRepository<Project> {

    @NotNull
    Project create(@NotNull String userId, @NotNull String name, @NotNull String description);

    @NotNull
    Project create(@NotNull String userId, @NotNull String name);

}